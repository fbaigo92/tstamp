/*!
 *  @file 	   tsRTC.c
 *  @brief     Text functions for formatting the RTC register data
 *  @details   Firmware functions for the LPC1769 RTC peripheral
 *
 *  @author    Federico Baigorria
 *  @version   0.1.0
 *  @date      06-18-2020
 *  @copyright GNU Public License.
 */

#include "tsRTC.h"

/** Private functions prototypes
 */

static void tsItoa(uint32_t, uint8_t *, uint8_t);
static void tsFormatdate(tsSetdate, uint8_t*, uint8_t);

/** Private functions
 */

/**
@fn static void scItoa(int scBase, char *scBuff, uint8_t isDecimal)
@detail Itoa for Screen Cycle. Base it's always decimal.

@warning No null terminated character control is applied for destiny string (scBuff) so user must assure
		 that enough space is allocated for it.

@param tsBase Integer to be converted.
@param *tsBuff Buffer to be allocated the ASCII text. It's null terminated.
@param tsDigits How many digits long is the integer to be converted
@return void
*/

static void tsItoa(uint32_t tsBase, uint8_t *tsBuff, uint8_t tsDigits){
	uint8_t tsRound;
	uint32_t tsAux;

	///! Convert whole digits to ASCII values
	for(tsRound = 0; tsRound < tsDigits; tsRound++){
		tsAux = tsBase % 10;
		*(tsBuff + tsDigits - tsRound - 1) = 0x30 + tsAux;
		tsBase = (tsBase - tsAux) / 10;
	}

	///! Null terminated
	*(tsBuff + tsRound) = '\0';
}

/**
@fn static void tsFormatdate(tsSetdate tsBase, uint8_t *tsBuff, uint8_t tsAddHour)
@detail Date formatter. Given a buffer with sufficient space (user must assure that) it will be filled with
a string as "MM-dd-YY HH:mm:ss" if the tsAddHour flag is set but if not it will save "MM-dd-YY"

@warning No null terminated character control is applied for destiny string (scBuff) so user must assure
		 that enough space is allocated for it.

@param tsBase Struct with the RTC time data separated by fields.
@param *tsBuff Buffer to be allocated the date text. It's null terminated.
@param tsAddHour It's a flag for formatting the date as "MM-dd-YY" or "MM-dd-YY HH:mm:ss".
@return void
*/

static void tsFormatdate(tsSetdate tsBase, uint8_t *tsBuff, uint8_t tsAddHour){
	tsItoa(tsBase.tsMonth, tsBuff, 2);
	tsBuff[2] = '-';
	tsItoa(tsBase.tsDay, tsBuff+3, 2);
	tsBuff[5] = '-';
	tsItoa(tsBase.tsYear, tsBuff+6, 4);

	if(tsAddHour){
		tsBuff[10] = ' ';
		tsItoa(tsBase.tsHour, tsBuff+11, 2);
		tsBuff[13] = ':';
		tsItoa(tsBase.tsMin, tsBuff+14, 2);
		tsBuff[16] = ':';
		tsItoa(tsBase.tsSeg, tsBuff+17, 2);
	}
}

/** Public functions
 */

/**
@fn void tsRTCInit()
@detail RTC initialization for LPC1769. This function should be called first before any other in the user's
		firmware/module initializations.

		As default interrupts are disabled.

@return void
*/

void tsRTCInit(){
	RTC_TIME_T tsOnreset;

	///! Hardware RTC init
	//Chip_RTC_Init(LPC_RTC);

	///! Set current time on first init time 01:00:00AM, 01/01/1999
	tsOnreset.time[RTC_TIMETYPE_SECOND]  = 0;
	tsOnreset.time[RTC_TIMETYPE_MINUTE]  = 0;
	tsOnreset.time[RTC_TIMETYPE_HOUR]    = 1;
	tsOnreset.time[RTC_TIMETYPE_DAYOFMONTH]  = 1;
	tsOnreset.time[RTC_TIMETYPE_DAYOFWEEK]   = tsFriday;
	tsOnreset.time[RTC_TIMETYPE_DAYOFYEAR]   = 1;
	tsOnreset.time[RTC_TIMETYPE_MONTH]   = tsJan;
	tsOnreset.time[RTC_TIMETYPE_YEAR]    = 1999;

	Chip_RTC_SetFullTime(LPC_RTC, &tsOnreset);

	///! Disable the RTC to generate interrupts
	Chip_RTC_CntIncrIntConfig(LPC_RTC, RTC_AMR_CIIR_IMSEC, DISABLE);

	/* Clear interrupt pending */
	//Chip_RTC_ClearIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE | RTC_INT_ALARM);

	/* Enable RTC interrupt in NVIC */
	//NVIC_EnableIRQ((IRQn_Type) RTC_IRQn);

	///! Enable RTC
	Chip_RTC_Enable(LPC_RTC, ENABLE);

	///! The RTC has been configured. Needs to be set to a valid time
	GPREG0 = FALSE;
}

/**
@fn void tsRTCSet(tsSetdate tsRTCFulltime)
@detail RTC set time function. This function should be called after tsRTCInit()
		As default interrupts disabled

@return void
*/

void tsRTCSet(tsSetdate tsRTCFulltime){
	RTC_TIME_T tsConfig;

	///! Set new time
	tsConfig.time[RTC_TIMETYPE_SECOND]  = tsRTCFulltime.tsSeg;
	tsConfig.time[RTC_TIMETYPE_MINUTE]  = tsRTCFulltime.tsMin;
	tsConfig.time[RTC_TIMETYPE_HOUR]    = tsRTCFulltime.tsHour;
	tsConfig.time[RTC_TIMETYPE_DAYOFMONTH]  = tsRTCFulltime.tsDay;
	tsConfig.time[RTC_TIMETYPE_DAYOFWEEK]   = tsRTCFulltime.tsDayow;
	tsConfig.time[RTC_TIMETYPE_DAYOFYEAR]   = tsRTCFulltime.tsDayoy;
	tsConfig.time[RTC_TIMETYPE_MONTH]   = tsRTCFulltime.tsMonth;
	tsConfig.time[RTC_TIMETYPE_YEAR]    = tsRTCFulltime.tsYear;

	Chip_RTC_Enable(LPC_RTC, DISABLE);
	Chip_RTC_SetFullTime(LPC_RTC, &tsConfig);

//	Chip_RTC_CntIncrIntConfig(LPC_RTC, RTC_AMR_CIIR_IMSEC, ENABLE);
//	Chip_RTC_ClearIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE | RTC_INT_ALARM);
//	NVIC_EnableIRQ((IRQn_Type) RTC_IRQn);

	Chip_RTC_Enable(LPC_RTC, ENABLE);
}

/**
@fn void tsGetFullDate(uint8_t* date)
@detail Copy the current date formatted as a string "MM-dd-YY HH:mm:ss"

@return void
*/

void tsGetFullDate(uint8_t* date){
	tsSetdate tsDate;

	tsDate.tsDay =  Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_DAYOFMONTH);
	tsDate.tsMonth = Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_MONTH);
	tsDate.tsYear =  Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_YEAR);

	tsDate.tsSeg = Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_SECOND);
	tsDate.tsMin = Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_MINUTE);
	tsDate.tsHour = Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_HOUR);

	tsFormatdate(tsDate,date,1);
}

uint32_t get_fattime(){
	/* Pack date and time into a DWORD variable */
	return ((Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_YEAR) - 1980) << 25)
		   | (Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_MONTH) << 21)
		   | (Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_DAYOFMONTH) << 16)
		   | (Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_HOUR) << 11)
		   | (Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_MINUTE) << 5)
		   | (Chip_RTC_GetTime(LPC_RTC, RTC_TIMETYPE_SECOND) >> 1);
}

/**
@fn void RTC_IRQHandler(void)
@detail IRQ function handler for the RTC interrupt. Commented as default, this
		should be configured by the user.

@return void
*/

/*void RTC_IRQHandler(void)
{
	static portBASE_TYPE xSwitchRequired;

	if (Chip_RTC_GetIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE)) {
		/* Clear pending interrupt */
/*		Chip_RTC_ClearIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE);
		xSemaphoreGiveFromISR(semRTC, &xSwitchRequired);
	}

	portEND_SWITCHING_ISR(xSwitchRequired);
}*/

//void tsFirstRTCInit(){
//	///! RTC init
//	Chip_RTC_Init(LPC_RTC);
//	//Chip_RTC_Enable(LPC_RTC, ENABLE);
//    //if(GPREG0 == 0){
//    	tsRTCInit();
//    //}
//
//	//if(GPREG0 == 0){
//		tsSetdate Test;
//		Test.tsDay = 11;
//		Test.tsDayow = tsSunday;
//		Test.tsDayoy = 286;
//		Test.tsHour = 21;
//		Test.tsMin = 05;
//		Test.tsSeg = 0;
//		Test.tsMonth = tsOct;
//		Test.tsYear = 2020;
//		tsRTCSet(Test);
//		GPREG0 = 1;
//	//}
//}
