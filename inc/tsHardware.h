/*!
 *  @file 	   tsHardware.h
 *  @brief     RTC hardware non volatile registers. May be replaced by the official LPCOpen libraries
 *  @details   This registers are used for saving non volatile flags / data.
 *
 *  @author    Federico Baigorria
 *  @date      03-24-2020
 *  @copyright GNU Public License.
 */

#ifndef __TSTAMP_INC_TSHARDWARE_H__
#define __TSTAMP_INC_TSHARDWARE_H__

///! < Real Time Clock register (RTC - 0x40024000)
///! < 0x40024000UL: Starting address for the RTC nonvolatile registers

#define 	GPREG	((volatile uint32_t *)0x40024044UL)

#define		GPREG0	GPREG[0]
#define		GPREG1	GPREG[1]
#define		GPREG2	GPREG[2]
#define		GPREG3	GPREG[3]
#define		GPREG4	GPREG[4]

#endif /* __TSTAMP_INC_TSHARDWARE_H__ */
