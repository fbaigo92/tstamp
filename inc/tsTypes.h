/*!
 *  @file 	   tsTypes.h
 *  @brief     Data types used for date formatting functions
 *
 *  @author    Federico Baigorria
 *  @version   0.1.0
 *  @date      06-18-2020
 *  @copyright GNU Public License.
 */

#ifndef __TIMESTAMP_INC_TSTYPES_H__
#define __TIMESTAMP_INC_TSTYPES_H__

#include <stdint.h>

typedef enum {
	tsMonday = 1,
	tsTuesday,
	tsWednesday,
	tsThursday,
	tsFriday,
	tsSaturday,
	tsSunday
}tsDow;	///! Day of the Week

typedef enum {
	tsJan = 1, tsFeb, tsMar, tsApr,
	tsMay, tsJun, tsJul, tsAug,
	tsSep, tsOct, tsNov, tsDec
}tsMonth;	///! Day of the Week

typedef struct {
	uint8_t tsSeg;
	uint8_t tsMin;
	uint8_t tsHour;
	uint8_t tsDay;
	tsMonth tsMonth;
	uint16_t tsYear;
	uint8_t tsDayoy;
	tsDow tsDayow;
} tsSetdate;


#endif /* __TIMESTAMP_INC_TSTYPES_H__ */
