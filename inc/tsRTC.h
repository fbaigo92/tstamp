/*!
 *  @file 	   tsRTC.h
 *  @brief     RTC time stamp functions for LPC1769.
 *  @details   User can set different parameters detailed below. For now no function uses any Free RTOS related
 *  		   functions.
 *
 *			   Header files:
 *			   tsTypes.h 	Data types for the date structure usage
 *			   tsHardware.h All hardware related definitions, macro and header files needed for
 *			   				the RTC non volatile registers. May soon be deprecated.
 *	@warning   Needs to be edited by the user accordingly
 *  @author    Federico Baigorria
 *  @date      03-24-2020
 *  @copyright GNU Public License.
 */

#ifndef __TS_RTC_LPC1769_H__
#define __TS_RTC_LPC1769_H__

///! User defined: If Free RTOS it's used for the RTC functions then set to 1 else 0
#define TS_FRTOS 	0

/** DO NOT EDIT
 */

///! Self header files
#if TS_FRTOS
#include "FreeRTOS.h"
#include "semphr.h"
#endif

#include "board.h"
#include "../../tstamp/inc/tsTypes.h"
#include "../../tstamp/inc/tsHardware.h"
#include "../../chip/inc/rtc_17xx_40xx.h"

///! For future use. See RTC_IRQHandler
// extern xSemaphoreHandle semRTC;

//! Public functions
void tsRTCInit();
void tsRTCSet(tsSetdate);
void tsGetFullDate(uint8_t * date);

#endif /* __TS_RTC_LPC1769_H__ */
